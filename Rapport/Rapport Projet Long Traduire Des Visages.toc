\babel@toc {french}{}
\contentsline {section}{\numberline {1}Remerciements}{2}{section.1}
\contentsline {section}{\numberline {2}Abstract}{2}{section.2}
\contentsline {section}{\numberline {3}Objectifs du projet}{3}{section.3}
\contentsline {section}{\numberline {4}Introduction}{3}{section.4}
\contentsline {section}{\numberline {5}Outils mis en place}{4}{section.5}
\contentsline {subsection}{\numberline {5.1}R\IeC {\'e}seaux de neurones}{4}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Perceptron Multi Couches}{4}{subsubsection.5.1.1}
\contentsline {paragraph}{Fonctionnement g\IeC {\'e}n\IeC {\'e}ral et propagation}{5}{section*.2}
\contentsline {paragraph}{Descente de gradient}{7}{section*.3}
\contentsline {paragraph}{R\IeC {\'e}tropropagation}{9}{section*.4}
\contentsline {subsubsection}{\numberline {5.1.2}R\IeC {\'e}seaux convolutifs}{10}{subsubsection.5.1.2}
\contentsline {paragraph}{Principe de fonctionnement}{11}{section*.5}
\contentsline {paragraph}{Transfer Learning}{13}{section*.6}
\contentsline {paragraph}{Augmentation du jeu de Donn\IeC {\'e}es}{13}{section*.7}
\contentsline {subsubsection}{\numberline {5.1.3}R\IeC {\'e}seaux r\IeC {\'e}currents}{15}{subsubsection.5.1.3}
\contentsline {paragraph}{Principes th\IeC {\'e}oriques}{15}{section*.8}
\contentsline {paragraph}{Diff\IeC {\'e}rents types de cellules}{15}{section*.9}
\contentsline {paragraph}{Cellules r\IeC {\'e}currentes \IeC {\'e}l\IeC {\'e}mentaires}{15}{section*.10}
\contentsline {paragraph}{Cellules LSTM}{16}{section*.11}
\contentsline {paragraph}{Cellules GRU}{17}{section*.12}
\contentsline {subsection}{\numberline {5.2}Traitement des Images}{19}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Isolation des l\IeC {\`e}vres}{19}{subsubsection.5.2.1}
\contentsline {paragraph}{Algorithme de Viola Jones}{19}{section*.13}
\contentsline {paragraph}{Image int\IeC {\'e}grale}{19}{section*.14}
\contentsline {paragraph}{Adaboost}{20}{section*.15}
\contentsline {paragraph}{Cascade}{20}{section*.16}
\contentsline {paragraph}{Algorithme basique}{20}{section*.17}
\contentsline {subsubsection}{\numberline {5.2.2}Auto-Encodeur}{21}{subsubsection.5.2.2}
\contentsline {subsection}{\numberline {5.3}Traitement du Langage}{21}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Word Embedding}{22}{subsubsection.5.3.1}
\contentsline {paragraph}{Principe g\IeC {\'e}n\IeC {\'e}ral}{22}{section*.18}
\contentsline {paragraph}{R\IeC {\'e}sultats}{23}{section*.19}
\contentsline {subsubsection}{\numberline {5.3.2}Seq-2-Seq}{24}{subsubsection.5.3.2}
\contentsline {section}{\numberline {6}D\IeC {\'e}veloppement du projet}{24}{section.6}
\contentsline {subsection}{\numberline {6.1}Pr\IeC {\'e}-traitement des Donn\IeC {\'e}es}{24}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Choix des ensembles de Donn\IeC {\'e}es}{24}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}\IeC {\'E}tude}{24}{subsubsection.6.1.2}
\contentsline {paragraph}{exemple de fichier de sous-titres :}{25}{section*.20}
\contentsline {subparagraph}{Etape 1 :}{25}{section*.21}
\contentsline {subparagraph}{Etape 2 :}{25}{section*.22}
\contentsline {subparagraph}{Etape 3 :}{25}{section*.23}
\contentsline {subparagraph}{Etape 4 :}{25}{section*.24}
\contentsline {subsubsection}{\numberline {6.1.3}Solution choisie}{26}{subsubsection.6.1.3}
\contentsline {subsection}{\numberline {6.2}Choix de la structure}{30}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Premi\IeC {\`e}re solution envisag\IeC {\'e}e}{30}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Deuxi\IeC {\`e}me solution envisag\IeC {\'e}e}{30}{subsubsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.3}Troisi\IeC {\`e}me solution envisag\IeC {\'e}e}{30}{subsubsection.6.2.3}
\contentsline {subsection}{\numberline {6.3}Mise en place de la structure}{31}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Langage Model}{31}{subsubsection.6.3.1}
\contentsline {paragraph}{Word embedding sous forme d'auto-encoder}{31}{section*.25}
\contentsline {subparagraph}{R\IeC {\'e}sultats : }{32}{section*.26}
\contentsline {paragraph}{Word embedding sous forme de matrices}{33}{section*.27}
\contentsline {subparagraph}{R\IeC {\'e}sultats : }{34}{section*.28}
\contentsline {subsubsection}{\numberline {6.3.2}Groupe B}{36}{subsubsection.6.3.2}
\contentsline {subsection}{\numberline {6.4}Analyse des r\IeC {\'e}sultats}{36}{subsection.6.4}
\contentsline {section}{\numberline {7}Voies d'am\IeC {\'e}liorations}{36}{section.7}
\contentsline {subsection}{\numberline {7.1}Capsule Network}{36}{subsection.7.1}
\contentsline {paragraph}{Les Capsule Networks dans le projet}{36}{section*.29}
\contentsline {paragraph}{La th\IeC {\'e}orie des Capsule Networks}{37}{section*.30}
\contentsline {subsection}{\numberline {7.2}Transformers/Attention}{38}{subsection.7.2}
\contentsline {section}{\numberline {8}Conclusion}{38}{section.8}
\contentsline {section}{\numberline {9}Bibliographie}{40}{section.9}
