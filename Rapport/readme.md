**Rappel de la procédure pour ajouter des morceaux au rapport :**

1.  Créer une nouvelle branche pour travailler sur sa partie
2.  Push **sur sa branche** les modifications au fur et à mesure
3.  Faire une Merge Request pour fusionner avec la branche rapport
4.  **ATTENDRE** que quelqu'un d'autre valide la merge request et vérifie qu'il n'y ait pas un écrasement des parties de quelqu'un d'autre, **après avoir rebase**