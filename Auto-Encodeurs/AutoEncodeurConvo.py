# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 08:16:45 2019

@author: Utilisateur
"""

import tensorflow as tf
import numpy as np
import os, sys
from tqdm import tqdm
import matplotlib.pyplot as plt
from time import time

pathname = os.path.dirname(sys.argv[0])        
path = os.path.abspath(pathname)            
mnist = tf.keras.datasets.mnist
tf.reset_default_graph()

(Xtrain, Ytrain),(Xtest, Ytest) = mnist.load_data()
Xtrain, Xtest = Xtrain / 255.0, Xtest / 255.0

XtrainChanel = [np.reshape(elt, (28, 28, 1)) for elt in Xtrain]
XtestChanel  = [np.reshape(elt, (28, 28, 1)) for elt in Xtest]

dimensionCode = 10

def batch(X, n):
    X = np.array(X)
    np.random.shuffle(X)
    n = len(X)//n
    return np.array(np.array_split(X, n))


x = tf.placeholder(tf.float32, shape = [None, 28, 28, 1])

conv1 = tf.layers.conv2d(
    x,
    32,
    [5, 5],
    padding='same',
    activation=tf.nn.relu,
    kernel_regularizer=None,
    bias_regularizer=None,
    activity_regularizer=None,
    )

pool1 = tf.layers.max_pooling2d(
        inputs = conv1, 
        pool_size = [2, 2], 
        strides = 2
        )

conv2 = tf.layers.conv2d(
      inputs=pool1,
      filters=64,
      kernel_size=[3, 3],
      padding="same",
      activation=tf.nn.relu
      )

pool2 = tf.layers.max_pooling2d(
        inputs = conv2, 
        pool_size = [2, 2], 
        strides = 2
        )


pool2_flat = tf.reshape(pool2, shape = [-1, 7 * 7 * 64])

"""
dense1 = tf.layers.dense(
    pool2_flat,
    1024,
    activation=tf.nn.relu,
)

dropout = tf.layers.dropout(
        dense1,
        0.25)
"""

mean_layer = tf.layers.dense(
    pool2_flat,
    10,
    activation = tf.identity,
)
"""
std_layer = tf.layers.dense(
    dropout,
    10,
    activation = tf.nn.softplus,
)
"""
code = mean_layer# + std_layer * (tf.random_normal(tf.shape(mean_layer), 0, 1, dtype = tf.float32))
"""
dense_exit = tf.layers.dense(
    code,
    1024,
    activation=tf.nn.relu,
)
"""
dense_exit2 = tf.layers.dense(
    code,
    7 * 7 * 64,
    activation=tf.nn.relu,
)



dense_exit2_flat = tf.reshape(dense_exit2, shape = [-1, 7, 7, 64])

unpool2 = tf.image.resize_images(dense_exit2_flat, size=(14, 14), method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

deconv2 = tf.layers.conv2d(
      inputs=unpool2,
      filters=32,
      kernel_size=[3, 3],
      padding="same",
      activation=tf.nn.relu
      )

unpool1 = tf.image.resize_images(deconv2, size=(28, 28), method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

deconv1 = tf.layers.conv2d(
      inputs        = unpool1,
      filters       = 1,
      kernel_size   = [5, 5],
      padding       = "same",
      activation    = tf.nn.relu
      )

ressemblance = tf.losses.mean_squared_error(deconv1, x)
optimizer = tf.train.AdamOptimizer(learning_rate = 0.01).minimize(ressemblance)

init = tf.global_variables_initializer()

nbEpoch = 5
results, cout = [], []

saver = tf.train.Saver(tf.global_variables())

with tf.Session() as sess:
    sess.run(init)


    for epoch in tqdm(range(nbEpoch)):
        Xbatch = batch(XtrainChanel, 1024)
        for XtrainBatch in tqdm(Xbatch):

            sess.run([optimizer], feed_dict = {x: XtrainBatch})
            
        c = sess.run([ressemblance], feed_dict = {x: XtestChanel})
        cout.append(c)
        results.append(deconv1.eval(feed_dict = {x: XtestChanel[50:60]}))  

def printRes(epoch):
    f, a = plt.subplots(2,10,figsize=(20,4))
    for i in range(10):
        a[0][i].imshow(Xtest[i+50])
        a[1][i].imshow(np.reshape(results[epoch][i], (28, 28)))