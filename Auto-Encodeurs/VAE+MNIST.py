#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 11:01:35 2019

@author: dedicad
"""
#%% Imports
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tqdm import tqdm

#Pour le moment on utilise Mnist pour essayer
from tensorflow.examples.tutorials.mnist import input_data
import tensorflow_probability as tfp

#%%Constantes
latentSpaceSize = 2
nbrEpochs = 2

#%%
tfd = tfp.distributions

def make_encoder(data, code_size):
    x = tf.layers.flatten(data)
    x = tf.layers.dense(x,200,tf.nn.relu)
    x = tf.layers.dense(x,200,tf.nn.relu)
    #Input Layer :
    loc = tf.layers.dense(x, code_size)
    #Output layer
    scale = tf.layers.dense(x, code_size, tf.nn.softplus)
    #Return the matric of weights
    return tfd.MultivariateNormalDiag(loc,scale)

def make_prior(code_size):
    '''The prior distribution is the initial distribution of the '''
    loc = tf.zeros(code_size)
    scale = tf.ones(code_size)
    return tfd.MultivariateNormalDiag(loc,scale)

#%% Then the decoder take a code and transform it toward an image
    
def make_decoder(code,data_shape):
    x = code
    x = tf.layers.dense(x,200,tf.nn.relu)
    x = tf.layers.dense(x,200,tf.nn.relu)
    logit = tf.layers.dense(x, np.prod(data_shape))
    logit = tf.reshape(logit, [-1] + data_shape)
    #On renvoie  une distribution de variable de Bernoulli qui correspond à l'image
    #Ce qui revient à modéliser les pixels en binaires (1 = blanc et 0 = noir)
    #Le Independant premet d'évaluer la proba d'une image sous une certaine distribution et non pas de 
    #considérer les pixels individuellement
    return tfd.Independent(tfd.Bernoulli(logit),2)
    
    
#Ceci permet de réutiliser plusieurs fois les fonction en les wrappant
make_encoder = tf.make_template('encoder', make_encoder)
make_decoder = tf.make_template('decoder', make_decoder)

#Le prior n'a pas de variables à entrainer, donc il n'y a pas besoin de le wrapper pour le réutiliser


#%% Fonction de coût
#La fonction utilisé ici : ELBO (evidence lower bound) est une approximation de la vraisemblance.
#On utilise la probabilité d'un point de données en fonction de notre estimation courante de sa valeur

data = tf.placeholder(tf.float32, [None, 28,28])

prior = make_prior(code_size=latentSpaceSize)
posterior = make_encoder(data, code_size=latentSpaceSize)
code = posterior.sample()  #On obtient ici le tensor de retour après passage à travers l'encodeur

#Pour obtenir la vraisemblance on décode le code obtenu que l'on normalise avec log_prob
likelihood = make_decoder(code,[28,28]).log_prob(data)
#Ici on utilise la divergence Kullback-Leibler pour mesure l'écart entre les deux distributions
#https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence

divergence = tfd.kl_divergence(posterior, prior)
#On explicite ce qu'il faut minimiser pour TensorFlow
elbo = tf.reduce_mean(likelihood-divergence)

#%% Training
#On choisit la méthode Adam pour la descente de gradient puisqu'on a dit qu'on utiliserait 
#Celle là a priori

optimize = tf.train.AdamOptimizer(0.001).minimize(-elbo)

#On injecte quelques échantillons de codes aléatoires pour visualiser les images correspondantes après décodage
#Cela nous permet d'isoler des caractéristiques/d'étudier le décodeur

samples = make_decoder(prior.sample(10),[28,28]).mean()

#%% Fonctions de visualisation
#J'ai récupéré des fonctions en lignes pour visualiser proprement nos résultats
def plot_codes(ax, codes, labels):
  ax.scatter(codes[:, 0], codes[:, 1], s=2, c=labels, alpha=0.1)
  ax.set_aspect('equal')
  ax.set_xlim(codes.min() - .1, codes.max() + .1)
  ax.set_ylim(codes.min() - .1, codes.max() + .1)
  ax.tick_params(
      axis='both', which='both', left=False, bottom=False,
      labelleft=False, labelbottom=False)


def plot_samples(ax, samples):
  for index, sample in enumerate(samples):
    ax[index].imshow(sample, cmap='gray')
    ax[index].axis('off')
#%% Session and run the training

mnist = input_data.read_data_sets('MNIST_data/')
fig, ax = plt.subplots(nrows=nbrEpochs, ncols=11, figsize=(10, nbrEpochs))

with tf.train.MonitoredSession() as sess:
    for epoch in tqdm(range(nbrEpochs)):
        feed = {data: mnist.test.images.reshape([-1, 28, 28])}
        test_elbo, test_codes, test_samples = sess.run([elbo, code, samples], feed)
        print('Epoch', epoch, 'elbo', test_elbo)
        ax[epoch, 0].set_ylabel('Epoch {}'.format(epoch))
#        plot_codes(ax[epoch, 0], test_codes, mnist.test.labels)
        plot_samples(ax[epoch, 1:], test_samples)
        for _ in range(600):
          feed = {data: mnist.train.next_batch(100)[0].reshape([-1, 28, 28])}
          sess.run(optimize, feed)
    #Visualisation du graphe avec TensorBoard
    writer = tf.summary.FileWriter('tmp/VAE,MNIST/2', sess.graph)
#%%
#Si on souhaite enregistrer les figures :
#plt.savefig('~/vae-mnist.png', dpi=300, transparent=True, bbox_inches='tight') #ne marche pas ...
    




    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    