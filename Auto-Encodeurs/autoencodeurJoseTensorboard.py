import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import matplotlib.image as mpimg
from tensorflow.contrib.layers import fully_connected
from tqdm import tqdm


#%%
"préparation des données"
path = 'lips//res//frame'
nframe=199

trainingLength = int(0.9*nframe)   #Nombre de frame que l'on garde pour l'entrainement, le reste sera pour le test
def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])

image1 = rgb2gray(mpimg.imread(path  + str(1) + '.jpg')) #Load the first image

imageSize = image1.shape
num_inputs=imageSize[0]*imageSize[1]  # taille image entrée si les images ont bien été resized

lips = np.zeros((nframe,imageSize[0], imageSize[1]))

for i in range(1,nframe-1):
#    print(len(mpimg.imread(path  + str(i+1) + '.jpg')))
#    print(len(mpimg.imread(path  + str(i+1) + '.jpg')[0]))
#    lips[i-1] = np.reshape(mpimg.imread(path  + str(i+1) + '.jpg',grayscale),(41*69,1))
#    Without resizing :
    lips[i-1] = rgb2gray(mpimg.imread(path  + str(i) + '.jpg'))
   
lipsReshaped = np.reshape(lips,(nframe,num_inputs))
lipsTrain= lipsReshaped[:trainingLength]
lipsTest= lipsReshaped[trainingLength:]

#%%
"initialisation de lautoencodeur"

num_hid1=1000               
num_hid2=500
num_hid3=num_hid1
num_output=num_inputs
lr=0.01         #learning rate
actf=tf.nn.relu   #fonction d'activation

X=tf.placeholder(tf.float32,shape=[None,num_inputs], name="X")
initializer=tf.variance_scaling_initializer()

with tf.name_scope("Layer1"): 
#    Ne pas mettre d'espace dans les noms de scope sinon ça ne fonctionne pas
    w1=tf.Variable(initializer([num_inputs,num_hid1]),dtype=tf.float32, name="w1")
    b1=tf.Variable(tf.zeros(num_hid1), name="b1")
    hid_layer1=actf(tf.matmul(X,w1)+b1)

with tf.name_scope("Layer2"):
    w2=tf.Variable(initializer([num_hid1,num_hid2]),dtype=tf.float32, name="w2")
    b2=tf.Variable(tf.zeros(num_hid2), name="b2")
    hid_layer2=actf(tf.matmul(hid_layer1,w2)+b2)

with tf.name_scope("Layer3"):
    w3=tf.Variable(initializer([num_hid2,num_hid3]),dtype=tf.float32, name="w3")
    b3=tf.Variable(tf.zeros(num_hid3), name="b3")
    hid_layer3=actf(tf.matmul(hid_layer2,w3)+b3)

with tf.name_scope("Layer4"):
    w4=tf.Variable(initializer([num_hid3,num_output]),dtype=tf.float32, name="w4")
    b4=tf.Variable(tf.zeros(num_output), name="b4")
    output_layer=actf(tf.matmul(hid_layer3,w4)+b4)
    
    
#%% Fonction de cout
loss=tf.reduce_mean(tf.square(output_layer-X)) #choix du type d'erreur
tf.summary.histogram("loss", loss)
tf.summary.scalar("loss", loss)

#%%Optimisation
optimizer=tf.train.AdamOptimizer(lr)  #choix optimizer
train=optimizer.minimize(loss)    

init=tf.global_variables_initializer()

#%%Entrainement du modèle
num_epoch=1              #nbre passage sur train
batch_size=1                #taille batch
num_test_images=nframe-trainingLength                      #taille test

"initialisation de la session"
lipsTensor = tf.reshape(lips, [nframe,imageSize[0], imageSize[1],1])

tf.summary.image('InputImages', lipsTensor,15)
summ = tf.summary.merge_all()

with tf.Session() as sess:
    sess.run(init)
    #Visualisation du graphe avec TensorBoard    
    
    writer = tf.summary.FileWriter('tmp/AutoEncodeur/4', sess.graph)
    
    
    for epoch in tqdm(range(num_epoch)):
        
        num_batches=len(lipsTrain)//batch_size
        for iteration in range(num_batches):            #boucle sur les batchs
            X_batch=lipsTrain[iteration*batch_size:(iteration+1)*batch_size]
            [result, s] = sess.run([train   ,summ],feed_dict={X:X_batch})
            writer.add_summary(s,epoch)

            
        train_loss=loss.eval(feed_dict={X:X_batch})
        print("epoch {} loss {}".format(epoch,train_loss))   #on affiche l'erreur après chaque passage sur Train
        
    results=output_layer.eval(feed_dict={X:lipsTest[:num_test_images]})     #on evalue sur test
    
    
    
    
#    "Comparaison Test et resultat"
#    f,a=plt.subplots(2,4,figsize=(20,4))
#    for i in range(num_test_images):
#        a[0][i].imshow(np.reshape(lipsTest[i],imageSize))
#        a[1][i].imshow(np.reshape(results[i],imageSize))
#    