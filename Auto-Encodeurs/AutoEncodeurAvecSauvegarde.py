﻿# -*- coding: utf-8 -*-
"""
Created on Sun Feb 17 19:39:25 2019

@author: Utilisateur
"""

import tensorflow as tf
import numpy as np
import os, sys
from tqdm import tqdm
import matplotlib.pyplot as plt

pathname = os.path.dirname(sys.argv[0])        
path = os.path.abspath(pathname)            


mnist = tf.keras.datasets.mnist

#Permet de créer n batchs
def batch(n):
    X = np.array(XtrainFlat)
    np.random.shuffle(X)
    return np.array_split(X, n)


tf.reset_default_graph()


(Xtrain, Ytrain),(Xtest, Ytest) = mnist.load_data()
Xtrain, Xtest = Xtrain / 255.0, Xtest / 255.0

XtrainFlat = [elt.flatten() for elt in Xtrain]
XtestFlat  = [elt.flatten() for elt in Xtest]


#Nombre de neuronnes par couches
nbNeuronneParCouche = [784, 200, 100]
dimensionCode = 10

#Couche d'input
x = tf.placeholder(tf.float32, [None, nbNeuronneParCouche[0]])
encoder = [x]
decoder = []

#Création d'une liste de couches 
for i, nbNeur in enumerate(nbNeuronneParCouche):
    hiddenLayer = tf.layers.dense(
        inputs = encoder[-1],
        units = nbNeur,
        activation = tf.nn.elu
        )
    
    encoder.append(hiddenLayer)
    
mean = tf.layers.dense(
        inputs = encoder[-1],
        units = dimensionCode,
        activation = tf.identity
        )

stddev = tf.layers.dense(
        inputs = encoder[-1],
        units = dimensionCode,
        activation = tf.nn.softplus
        )

code = mean + stddev * (tf.random_normal(tf.shape(mean), 0, 1, dtype = tf.float32))

hiddenLayer = tf.layers.dense(
    inputs = code,
    units = nbNeuronneParCouche[-1],
    activation=tf.nn.elu
    )

decoder.append(hiddenLayer)


nbNeuronneParCouche.reverse()
for i, nbNeur in enumerate(nbNeuronneParCouche[1:-1]):
    hiddenLayer = tf.layers.dense(
        inputs = decoder[-1],
        units = nbNeur,
        activation=tf.nn.elu
        )
    decoder.append(hiddenLayer)
    
outputLayer = tf.layers.dense(
    inputs = decoder[-1],
    units = nbNeuronneParCouche[-1],
    activation=tf.nn.sigmoid
    )

decoder.append(outputLayer)

    

#Focntion de coût est la norme de la différence au carré
#ressemblance = tf.losses.mean_squared_error(decoder[-1], x)
ressemblance = tf.losses.mean_squared_error(decoder[-1], x)
KLDivergence = 0.001 * tf.reduce_sum((tf.square(mean) + tf.square(stddev))/2 - (1*tf.log(1e-8 + stddev)) - 1/2, 1)

ELBO = - ressemblance - tf.reduce_mean(KLDivergence) 

optimizer = tf.train.AdamOptimizer(learning_rate=0.001).minimize(-ELBO)


init = tf.global_variables_initializer()

nbEpoch = 200
results, cout = [], []



saver = tf.train.Saver(tf.global_variables())

with tf.Session() as sess:
    sess.run(init)
    
    yn = None
    while yn != "y" and yn != "n":
        yn = input("Charger une session ? y/n ")
    if yn == "y":
        nom = input("Nom de la suavegarde\n")
        saver.restore(sess, path + "\\sess\\" + nom) 

        
        
    #Entrainement    
    for epoch in tqdm(range(nbEpoch)):
        Xbatch = batch(1024)
        for XtrainBatch in Xbatch:
            sess.run([optimizer], feed_dict = {x: XtrainBatch})
        c = sess.run([-ELBO], feed_dict = {x: XtestFlat})
        cout.append(c)
        results.append(decoder[-1].eval(feed_dict = {x: XtestFlat[50:60]}))
        
    
    print(c)
    yn = None
    while yn != "y" and yn != "n":
        yn = input("Sauvegarder la session ? y/n ")
    
    if yn == "y":
        nom = input("Nom de la sauvegarde ?\n")
        save_path_save = saver.save(sess, path + "\\sess\\" + nom)
    
    error = sess.run([-ELBO], feed_dict = {x: XtestFlat})
    
    
    
    
    
def printRes(epoch):
    f, a = plt.subplots(2,10,figsize=(20,4))
    for i in range(10):
        a[0][i].imshow(Xtest[i+50])
        a[1][i].imshow(np.reshape(results[epoch][i], (28, 28)))
            
        
plt.close()
plt.plot(range(nbEpoch), cout)
plt.show()







