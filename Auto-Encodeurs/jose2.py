import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import matplotlib.image as mpimg
from tensorflow.contrib.layers import fully_connected

"préparation des données"
nframe=5850
lips=[0]*nframe
for i in range(0,nframe-1):
    print(len(mpimg.imread('lips//frame ('  + str(i+1) + ').jpg')))
    print(len(mpimg.imread('lips//frame ('  + str(i+1) + ').jpg')[0]))
    lips[i-1] = np.reshape(mpimg.imread('lips//frame ('  + str(i+1) + ').jpg'),(36*59,1))
lipsTrain=lips[:5000]
LipsTest=lips[5000:]

B_neck=[10,50,100,150,200,250,300,400,500,750,1000,1500]
error=[0 for k in range(len(B_neck))]

for i in range(len(B_neck)):
    "initialisation de lautoencodeur"
    num_inputs=35*58    #35x58 pixels taille image entrée
    num_hid1=1000               
    num_hid2=B_neck[i]
    num_hid3=num_hid1
    num_output=num_inputs
    lr=0.01         #learning rate
    actf=tf.nn.relu   #fonction d'activation
    
    X=tf.placeholder(tf.float32,shape=[None,num_inputs])
    initializer=tf.variance_scaling_initializer()
    
    w1=tf.Variable(initializer([num_inputs,num_hid1]),dtype=tf.float32)
    w2=tf.Variable(initializer([num_hid1,num_hid2]),dtype=tf.float32)
    w3=tf.Variable(initializer([num_hid2,num_hid3]),dtype=tf.float32)
   w:4=tf.Variable(initializer([num_hid3,num_output]),dtype=tf.float32)
    
    b1=tf.Variable(tf.zeros(num_hid1))
    b2=tf.Variable(tf.zeros(num_hid2))
    b3=tf.Variable(tf.zeros(num_hid3))
    b4=tf.Variable(tf.zeros(num_output))
    
    hid_layer1=actf(tf.matmul(X,w1)+b1)
    hid_layer2=actf(tf.matmul(hid_layer1,w2)+b2)
    hid_layer3=actf(tf.matmul(hid_layer2,w3)+b3)
    output_layer=actf(tf.matmul(hid_layer3,w4)+b4)
    
    loss=tf.reduce_mean(tf.square(output_layer-X)) #choix du type d'erreur
    
    optimizer=tf.train.AdamOptimizer(lr)  #choix optimizer
    train=optimizer.minimize(loss)    
    
    init=tf.global_variables_initializer()
    
    
    num_epoch=5              #nbre passage sur train
    batch_size=150                 #taille batch
    num_test_images=10                      #taille test
    
    "initialisation de la session"
    
    
    with tf.Session() as sess:
        sess.run(init)
        for epoch in range(num_epoch):
            
            num_batches=len(lipsTrain)//batch_size
            for iteration in range(num_batches):            #boucle sur les batchs
                X_batch=lipsTrain[iteration*batch_size:(iteration+1)*batch_size]
                sess.run(train,feed_dict={X:X_batch})
                
            train_loss=loss.eval(feed_dict={X:X_batch})
            print("epoch {} loss {}".format(epoch,train_loss))   #on affiche l'erreur après chaque passage sur Train
            
        results=output_layer.eval(feed_dict={X:lipsTest[:num_test_images]})     #on evalue sur test
        
        "Comparaison Test et resultat"
        f,a=plt.subplots(2,10,figsize=(20,4))
        for i in range(num_test_images):
            a[0][i].imshow(np.reshape(lipsTest[i],(35,58)))
            a[1][i].imshow(np.reshape(results[i],(35,58)))
        error[i]=train_loss
