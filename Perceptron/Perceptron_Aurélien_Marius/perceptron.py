import numpy as np
from random import *
from tqdm import tqdm
import matplotlib.pyplot as plt




def affiche(mat):
    '''Cette fonction permet d'afficher une matrice proprement'''
    for ligne in mat: print (ligne)
def transpose(mat):
    '''Cette fonction retourne la transposée de la matrice mat'''
    return [[mat[j][i] for j in range(len(mat))] for i in range(len(mat[0]))]

class perceptron(object):
    '''Classe pour créer un perceptron'''
    
    def __init__(self, listeNombreCellule, poids=[]):
        '''On fournit une liste comportant pour chaque couche le nombre de cellule (et donc le nombre de couches aussi)'''
        self.listeNombreCellule = listeNombreCellule
        self.neurones = []
        self.preNeurones = []
        self.taux = 2
        if poids == []:
            self.poids = []
            for m in range(len(listeNombreCellule)-1):
                self.poids.append(np.array([[(np.random.randn()) for i in range(
                    listeNombreCellule[m]+1)] for j in range(listeNombreCellule[m+1])]))
        else:
            self.poids = poids
        for couche in listeNombreCellule:
            self.neurones.append(np.zeros(couche+1))
            self.neurones[-1][-1] = 1
            self.preNeurones.append(np.zeros(couche+1))
            self.preNeurones[-1][-1] = 1

    def reset(self,listeNombreCellule, poids=[]):
        self.neurones = []
        self.preNeurones = []
        if poids == []:
            self.poids = []
            for m in range(len(listeNombreCellule)-1):
                self.poids.append(np.array([[5*(random()*2-1) for i in range(
                    listeNombreCellule[m]+1)] for j in range(listeNombreCellule[m+1])]))
        else:
            self.poids = poids
        for couche in listeNombreCellule:
            self.neurones.append(np.zeros(couche+1))
            self.neurones[-1][-1] = 1
            self.preNeurones.append(np.zeros(couche+1))
            self.preNeurones[-1][-1] = 1
            
    def sigmoid(self, z):
        '''Fonction d'activation d'un neurone'''
        return 1/(1+np.exp(-z))
        
    def sigmoidDerivee(self,sigmaZ):
        return sigmaZ * (1-sigmaZ)
        
    def erreur(self,donneesSortie):
        return np.sum(np.square(self.neurones[-1][:-1] - np.array(donneesSortie)))/(2*len(donneesSortie))
        
    def etat(self):
        return self.neurones

    def propagation(self, donneesEntree):
        self.neurones[0][:-1] = donneesEntree
        for m in range(len(self.neurones)-1):
            temp = np.matmul(self.poids[m],self.neurones[m])
            for k in range(len(temp)):
                elm = temp[k]
                self.preNeurones[m+1][k] = elm
                self.neurones[m+1][k] = self.sigmoid(elm)
                
    def update(self,tabValeur,indiceCoucheSortie):
        '''En entrée on fournit le gradient dans l'argument tabValeur et l'indice de la couche de sortie'''
        nbrLigne = len(self.poids[indiceCoucheSortie-1]) # Nombre de lignes de la matrice de poids
        nbrColonne = len(self.poids[indiceCoucheSortie-1][0]) # Nombre de colonnes de la matrice de poids
        
        for i in range(nbrLigne):
            for j in range(nbrColonne):
                self.poids[indiceCoucheSortie-1][i][j] -= self.taux*tabValeur[i][j]
        nbrNeuronesEntree = len(self.neurones[indiceCoucheSortie-1])-1
        valeursIdeales = []
        for i in range(nbrNeuronesEntree):      
            valeursIdeales.append(sum([self.neurones[indiceCoucheSortie-1][i]-
                                        tabValeur[j][i+nbrColonne] 
                                        for j in range(nbrLigne)])/nbrLigne)
        return valeursIdeales
        
    def retropropagation(self,donneesSortie,k):
        nbrNeuronesSortie = len(self.neurones[k])-1 # On ne compte pas le biais
        
        nbrNeuronesPere = len(self.neurones[k-1])-1 # On ne compte pas le biais
        gradient = [np.zeros(nbrNeuronesPere*2+1) for ns in range(nbrNeuronesSortie)] 
        # Gradient est une matrice dont chaque colonne correspont à un neurone de sortie fixé, au début de la
        # Colonne on trouve les nouveaux poids et ensuite les valeurs souhaitées pour les neurones pères
        
        # ns servira à faire varier le neurone d'arrivé (dans la couche k)
        # Calcul pour le neurone ns de la couche de sortie
        for ns in range(nbrNeuronesSortie):
            # print('ns:')
            # print(ns)
            # Calcul pour les poids
            for i in range(nbrNeuronesPere+1):
                # if ns==1:
                    # print(self.neurones[k-1][i])
                gradient[ns][i] = (self.neurones[k][ns]-donneesSortie[ns])*self.neurones[k-1][i]*self.sigmoidDerivee(self.neurones[k][ns])
                # print(":i")
                # print(i)
                # 
            #Boucle pour le calcul des Neurones
            for i in range(nbrNeuronesPere):
                    # Il y a nbrNeuronesPeres + 1 poids mais il n'y a que nbrNeuronesPeres neurones à ajuster 
                    # puisque le biais compte comme un poids mais on n'ajuste pas sa valeur en tant que neurone
                gradient[ns][i+nbrNeuronesPere+1] = (self.neurones[k][ns]-donneesSortie[ns])*self.poids[k-1][ns][i]*self.sigmoidDerivee(self.neurones[k][ns])
        return self.update(gradient,k)
    
    def apprentissage(self,donnees):
        # indice servira pour faire varier les couches ensuite. Cela représente l'indice de la couche d'arrivée
        entree = donnees
        for indice in range(len(self.neurones)-1,0,-1):
            entree = self.retropropagation(entree,indice)
        
    def apprendSet(self,setDonnees):
        '''Cette fonction prend en entrée un set de Données sous forme d'une liste de liste ou chaque liste             contient une liste avec les données d'entrée en position 0 et une liste avec les valeurs de sorties en position 1. Ex pour le xor : [[[1,1], [0]],
                    [[1,0], [1]],
                    [[0,1], [1]],
                    [[0,0], [0]]]'''
    #On commence par prendre un batch de 1
        
        for data in setDonnees:
            self.propagation(data[0])
            self.apprentissage(data[1])
    def resultat(self,donnees):
        self.propagation(donnees[0])
        print('neurones :')
        print(self.neurones[-1][:-1]) #On affiche la dernière couche de Neurones en enlevant le biais
        print ('resultat attendu :', donnees[1])
    
    def verification(self,setDonnees):
        listeErreur = []
        for data in setDonnees:
            self.propagation(data[0])
            listeErreur += [self.erreur(data[1])]
        return sum(listeErreur)/len(setDonnees)
        
if __name__=="__main__" : 
    poidsTheo = [transpose(np.array([[-0.31271959, -1.22006486, -0.02863739],
           [-0.17394585, -0.85849702, -0.19461938],
           [-0.87754955, -0.89073197,  2.22737199]])), transpose(np.array([[-0.46563202],
           [-0.12006771],
           [-0.78827869],
           [-1.5952977 ]]))]
    # affiche(poidsTheo)
    # print ('')
    
    
    petitPerceptron = perceptron([2, 3, 1], poidsTheo)
    affiche(petitPerceptron.poids)
    print ('ici')
    petitPerceptron.propagation([1, 0])
    print ('neurones :')
    affiche(petitPerceptron.neurones)
    petitPerceptron.apprentissage([1])
    print ('fin')
    print('poids')
    affiche(petitPerceptron.poids)
    
    setXor = [[[1,1], [0]],
        [[1,0], [1]],
        [[0,1], [1]],
        [[0,0], [0]]]*1000
    # petitPerceptron.apprendSet(setXor)
    # print (petitPerceptron.verification(setXor))
    #learningRate = []
    #listeErreur = []
    #for compteur in tqdm(range(20)):
    #    # petitPerceptron.reset([2, 3, 1])
    #    petitPerceptron.taux += 0.05
    #    # learningRate += [petitPerceptron.taux]
    #    petitPerceptron.apprendSet(setXor)
    #    listeErreur += [petitPerceptron.verification(setXor[:250])]
    #    # print ('taux : ', petitPerceptron.taux)
    #    # print ('erreur :',listeErreur[-1])
    #
    #
    #plt.plot(listeErreur)
    #plt.show()
    
    
    ###Tracé courbes run=100/ 
    #CF=[]
    #nrun=100
    #for run in tqdm(range(nrun)):
    #    petitPerceptron.taux=0.1
    #    listeErreur=[]
    #    for k in range(1,50): 
    #        petitPerceptron.reset([2, 4, 1])
    #        petitPerceptron.apprendSet(setXor[:100*k])
    #        listeErreur += [petitPerceptron.verification(setXor[:250])]
    #        
    #    CF += [listeErreur]
    #mat= np.array(CF)
    #mat2=np.transpose(CF)
    #        
    #moy=[0 for k in range(len(CF[0]))]
    #et=[0 for k in range(len(CF[0]))]
    #et1=[0 for k in range(len(CF[0]))]
    #et2=[0 for k in range(len(CF[0]))]
    #for k in range(len(CF[0])):
    #    moy[k]=np.mean(mat2[k])
    #    et[k]=np.std(mat2[k])
    #    et1[k]=moy[k]-2*et[k]/(np.sqrt(len(CF[0])))  #intervalle conf 95%
    #    et2[k]=moy[k]+2*et[k]/(np.sqrt(len(CF[0])))
    #x=[100*k for k in range(1,50)]
    #plt.plot(x,moy,"r--",label="moyenne")
    #plt.plot(x,et1,"b")
    #plt.plot(x,et2,"b")
    #plt.title("Erreur en fonction de l'apprentissage")
    #
    #plt.show()
    
    
    ##############################################################################


