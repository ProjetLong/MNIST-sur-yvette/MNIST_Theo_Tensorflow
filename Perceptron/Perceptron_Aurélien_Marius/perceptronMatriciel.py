import numpy as np
from random import *
from tqdm import tqdm
import matplotlib.pyplot as plt
from copy import deepcopy



def affiche(mat):
    '''Cette fonction permet d'afficher une matrice proprement'''
    for ligne in mat: print (ligne)
def transpose(mat):
    '''Cette fonction retourne la transposée de la matrice mat'''
    return [[mat[j][i] for j in range(len(mat))] for i in range(len(mat[0]))]

class perceptron(object):
    '''Classe pour créer un perceptron'''
    
    def __init__(self, listeNombreCellule, poids=[],taux =0.1):
        '''On fournit une liste comportant pour chaque couche le nombre de cellule (et donc le nombre de couches aussi)'''
        self.listeNombreCellule = listeNombreCellule
        self.taux = taux
        self.reset(listeNombreCellule, poids)
        self.sigmoid = np.vectorize(self.sigmoidByElement)
        self.sigmoidDerivee = np.vectorize(self.sigmoidDeriveeByElement)

    def reset(self,listeNombreCellule, poids=[]):
        self.neurones = []
        self.preNeurones = []
        if poids == []:
            self.poids = []
            for m in range(len(listeNombreCellule)-1):
                self.poids.append(np.array([[(np.random.randn()) for i in range(listeNombreCellule[m+1])] for j in range(listeNombreCellule[m]+1)]))
        else:
            self.poids = poids
        for couche in listeNombreCellule:
            self.neurones.append(np.zeros(couche+1))
            self.neurones[-1][-1] = 1
            self.preNeurones.append(np.zeros(couche+1))
            self.preNeurones[-1][-1] = 1
            
    def sigmoidByElement(self, z):
        '''Fonction d'activation d'un neurone'''
        return 1/(1+np.exp(-z))
        
    def sigmoidDeriveeByElement(self,sigmaZ):
        return sigmaZ * (1-sigmaZ)
        
    def erreur(self,donneesSortie):
        return np.sum(np.square(self.neurones[-1][:-1] - np.array(donneesSortie)))/(2*len(donneesSortie))
        
    def etat(self):
        return self.neurones

    def propagation(self, donneesEntree):
        self.neurones[0][:-1] = donneesEntree
        
        for m in range(len(self.neurones)-1):                          #On itère sur toutes les couches
            temp = np.matmul(self.neurones[m],self.poids[m])
            self.preNeurones[m+1]=temp
            self.neurones[m+1]=self.sigmoid(temp)
            self.preNeurones[m+1] = np.append(self.preNeurones[m+1],1) #Add the bias
            self.neurones[m+1] = np.append(self.neurones[m+1],1)       #Add the bias
                
    def update(self,gradNeurones,gradPoids,indiceCoucheSortie):
        '''En entrée on fournit les gradient et l'indice de la couche de sortie'''
        
        self.poids[indiceCoucheSortie - 1] -= self.taux*gradPoids.T    #Mise a jour des poids
        
        nbrNeuronesEntree = len(self.neurones[indiceCoucheSortie-1])-1
        nbrNeuronesSortie = len(self.neurones[indiceCoucheSortie])-1
        
        #Calcul de la valeur idéale pour chaque neurone
        somme = [sum(gradNeurones[i])/nbrNeuronesSortie for i in range(nbrNeuronesEntree)]
        valeursIdeales = self.neurones[indiceCoucheSortie-1][:-1]-somme         
        
        return valeursIdeales
        
    def retropropagation(self,donneesSortie,k):
        '''On considère que donneesSortie est une liste de la longueur du batch'''
        
        nbrNeuronesSortie = len(self.neurones[k])-1 # On ne compte pas le biais
        nbrNeuronesPere = len(self.neurones[k-1])-1 # On ne compte pas le biais
        tailleBatch = len(donneesSortie)
        
        gradNeurones = np.zeros((tailleBatch,nbrNeuronesPere+1,nbrNeuronesSortie))
        gradPoids = np.zeros((tailleBatch,nbrNeuronesSortie,nbrNeuronesPere+1))
        print ('taille', tailleBatch)
        
        transposeePoids = self.poids[k-1].T
        for indice in range(len(donneesSortie)) :
            element = donneesSortie[indice]
            intermediaire = np.multiply((self.neurones[k][:-1]-element),
                                        self.sigmoidDerivee(self.neurones[k][:-1]))
            print (gradNeurones)
            print ('hoy : ', np.multiply(intermediaire,transposeePoids).T)
            gradNeurones[indice] = np.multiply(intermediaire,transposeePoids).T
            
            
            # ns sert à faire varier le neurone d'arrivé (dans la couche k)
            for ns in range(nbrNeuronesSortie):
                gradPoids[indice][ns] = intermediaire[ns]*self.neurones[k-1]
        print ('Neurones : ', gradNeurones)
        print ('poids :', gradPoids)
        resNeurones = sum(gradNeurones)/tailleBatch
        resPoids = sum(gradPoids)/tailleBatch
        print ('resNeurones',resNeurones)
        print ('resPoids',resPoids)
        return self.update(resNeurones,resPoids,k)   
    
    def apprentissage(self,donnees):
        # indice servira pour faire varier les couches ensuite. Cela représente l'indice de la couche d'arrivée
        entree = donnees
        for indice in range(len(self.neurones)-1,0,-1):
            entree = self.retropropagation(entree,indice)
        
    def apprendSet(self,setDonnees):
        '''Cette fonction prend en entrée un set de Données sous forme d'une liste de liste ou chaque liste             contient une liste avec les données d'entrée en position 0 et une liste avec les valeurs de sorties en position 1. Ex pour le xor : [[[1,1], [0]],
                    [[1,0], [1]],
                    [[0,1], [1]],
                    [[0,0], [0]]]'''
    #On commence par prendre un batch de 1
        print ("Initialisation de l'apprentissage du set")
        for data in tqdm(setDonnees):
            self.propagation(data[0])
            self.apprentissage(data[1])
            
    def resultat(self,donnees):
        self.propagation(donnees[0])
        print('neurones :')
        print(self.neurones[-1][:-1]) #On affiche la dernière couche de Neurones en enlevant le biais
        print ('resultat attendu :', donnees[1])
    
    def verification(self,setDonnees):
        listeErreur = []
        print ('Initialisation de la vérification')
        for data in setDonnees:
            self.propagation(data[0])
            listeErreur += [self.erreur(data[1])]
        return sum(listeErreur)/len(setDonnees)
    
    def precision(self,setDonnees):
        listeErreur = 0
        # tqdm.write('Initialisation du calcul de la précision')
        for data in setDonnees:
            self.propagation(data[0])
            maxi = np.argmax(self.neurones[-1][:-1])
            if maxi == np.argmax(data[1]) and self.neurones[-1][:-1][maxi] > 0.5:
                listeErreur += 1
        return listeErreur/len(setDonnees)
if __name__=="__main__" : 
    poidsTheo = [np.array([[-0.31271959, -1.22006486, -0.02863739],
       [-0.17394585, -0.85849702, -0.19461938],
       [-0.87754955, -0.89073197,  2.22737199]]), np.array([[-0.46563202],
       [-0.12006771],
       [-0.78827869],
       [-1.5952977 ]])]
    
    petitPerceptron = perceptron([2, 3, 1], poidsTheo, 2)
    affiche(petitPerceptron.poids)
    print ('ici')
    petitPerceptron.propagation([1, 0])
    print ('neurones :')
    affiche(petitPerceptron.neurones)
    petitPerceptron.apprentissage([1])
    print ('fin')
    print('poids')
    affiche(petitPerceptron.poids)
    
    setXor = [[[[1,1], [0]],
        [[1,0], [1]],
        [[0,1], [1]],
        [[0,0], [0]]]]*1000
    # petitPerceptron.apprendSet(setXor)
    # print (petitPerceptron.verification(setXor))
    # learningRate = []
    # listeErreur = []
    # for compteur in tqdm(range(2)):
    #    # petitPerceptron.reset([2, 3, 1])
    #    petitPerceptron.taux += 0.05
    #    # learningRate += [petitPerceptron.taux]
    #    petitPerceptron.apprendSet(setXor)
    #    listeErreur += [petitPerceptron.verification(setXor[:250])]
    #    # print ('taux : ', petitPerceptron.taux)
    #    # print ('erreur :',listeErreur[-1])
    # 
    # 
    # plt.plot(listeErreur)
    # plt.show()
    
    
    ###Tracé courbes run=100/ 
    # CF=[]
    # nrun=1
    # for run in tqdm(range(nrun)):
    #    petitPerceptron.taux=0.1
    #    listeErreur=[]
    #    for k in range(1,50): 
    #        petitPerceptron.reset([2, 4, 1])
    #        petitPerceptron.apprendSet(setXor[:100*k])
    #        listeErreur += [petitPerceptron.verification(setXor[:250])]
    #        
    #    CF += [listeErreur]
    # mat= np.array(CF)
    # mat2=np.transpose(CF)
    #        
    # moy=[0 for k in range(len(CF[0]))]
    # et=[0 for k in range(len(CF[0]))]
    # et1=[0 for k in range(len(CF[0]))]
    # et2=[0 for k in range(len(CF[0]))]
    # for k in range(len(CF[0])):
    #    moy[k]=np.mean(mat2[k])
    #    et[k]=np.std(mat2[k])
    #    et1[k]=moy[k]-2*et[k]/(np.sqrt(len(CF[0])))  #intervalle conf 95%
    #    et2[k]=moy[k]+2*et[k]/(np.sqrt(len(CF[0])))
    # x=[100*k for k in range(1,50)]
    # plt.plot(x,moy,"r--",label="moyenne")
    # plt.plot(x,et1,"b")
    # plt.plot(x,et2,"b")
    # plt.title("Erreur en fonction de l'apprentissage")
    # 
    # plt.show()
    # 
    
    ##############################################################################


