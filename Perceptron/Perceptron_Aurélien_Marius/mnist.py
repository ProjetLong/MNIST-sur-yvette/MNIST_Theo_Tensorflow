from perceptronMatriciel import *
from sklearn.datasets import load_digits
from tqdm import tqdm
"""MNIST digits.data liste des pixels entree (64 chaque set)
        digits.target[i] correspond au label de digits.data[i]
        Il y a 1797 echantillons"""

digits = load_digits()

print ('ok')
if __name__ == '__main__':
    print ('hey')
    digits.data=(digits.data/16) #normalisation centrage autour de 0
    perceptronAurelien = perceptron([64, 20, 20 , 10],taux=0.1)
    DataSet=[0 for _ in range(len(digits.data))]
    for k in tqdm(range(len(digits.data))):
        sortie = np.zeros(10)
        sortie[digits.target[k]] = 1
        DataSet[k]=[digits.data[k],sortie]
    listeErreur = []
    # for k in tqdm(range(1,10)):
    #     perceptronAurelien.apprendSet(DataSet[10*(k-1):10*k])
    #     listeErreur += [perceptronAurelien.precision(DataSet[1200:])]
    nrun=15
    listerun=[0 for k in range(nrun)]
    
    for run in range(nrun):
        npass=150    
        Prec=[0 for k in range(npass)] 
        perceptronAurelien = perceptron([64, 20, 20 , 10],taux=0.1)
        for k in range(npass):
            perceptronAurelien.apprendSet(DataSet[:1200])
            print (perceptronAurelien.precision(DataSet[1200:]))
            print(k)
            Prec[k]=perceptronAurelien.precision(DataSet[1200:])
        listerun[run]=Prec
    print(listerun)
    mat= np.array(listerun)
    mat2=np.transpose(listerun)
            
    moy=[0 for k in range(npass)]
    et=[0 for k in range(npass)]
    et1=[0 for k in range(npass)]
    et2=[0 for k in range(npass)]
    for k in range(npass):
        moy[k]=np.mean(mat2[k])
        et[k]=np.std(mat2[k])
        et1[k]=moy[k]-1.96*et[k]/(np.sqrt(nrun))  #intervalle conf 95%
        et2[k]=moy[k]+1.96*et[k]/(np.sqrt(nrun))
    
    plt.figure
    plt.plot(moy,'r-',label="moyenne")
    for k in range(npass):
        plt.plot([k,k],[et1[k],et2[k]],'b-')
    
    plt.title("Precision en fonction du set d'entrainement")
    plt.ylabel('Précision')
    plt.xlabel("Taille Set d'entrainement")
    plt.show()
    
    
    
    