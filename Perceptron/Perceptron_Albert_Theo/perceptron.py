# -*- coding: utf-8 -*-
'''
Created on 7 juil. 2018

@author: Theo Moutakanni

'''

import numpy as np
import sympy as sym

from abc import ABC, abstractmethod

class Layer(ABC):
    """Classe abstraite Layer
    
    Classe abstraite servant de base a toutes les classes representant des couches (layer).
    """
    
    def __init__(self):
        """Example function with types documented in the docstring.

        Parameters
        ----------
        param1 : int
            The first parameter.
        param2 : str
            The second parameter.
    
        Returns
        -------
        bool
            True if successful, False otherwise.
    
        """
        super().__init__()
    
    @abstractmethod
    def output(self):
        """
        .. math::
             x(n) * y(n) \Leftrightarrow X(e^{j\omega } )Y(e^{j\omega } )
        """
        pass
    
    @abstractmethod
    def derivate(self,W,err):
        pass
    
    @abstractmethod
    def update(self):
        pass
    
    def addBias(self,M,bias=1):
        """ Ajoute du biais a une sortie
    
        Concatene bias a la fin de chaque lign
        M      -> (batch,n)
        output -> (batch,n+1
    
            Args:
                M (matrice): La matrice a laquelle il faut ajouter le biais, de taile (batch,n)
                bias, optional (float): Le biais a ajouter
        
            Returns:
                matrice: La matric M avec le biais, de taille (batch,n+1)
    
        """
        return np.array([np.concatenate((m,[bias])) for m in M])
    
    def removeBias(self,output):
        return np.array([o[:-1] for o in output])

class ConstLayer(Layer):
    def __init__(self, isize):
        """
        isize -> taille des observations
        """
        super().__init__()
        
        self.shape = isize
        self.Z = None
        self.f = lambda x:x
    
    def output(self,X,train=False):
        self.Z = X
        return self.Z
    
    def derivate(self,W,err):
        return np.zeros(self.shape)
    
    def update(self,W,err,coef,regularization=None,momentum=0.):
        pass

class FCLayer(Layer):

    def __init__(self, X, nsize, activation=None, dropout=0., name=None):
        """ 
        nsize = n_i+1, le nombre de neurones de la couche actuelle
        X -> (batch,n_i)
        W -> (n_i,n_i+1)
        Y -> (batch,n_i+1)
        """
        
        super().__init__()
        
        self.name = name
        self.X = X
        self.isize = X.shape
        self.shape = nsize

        self.W = self.init_weights()
        self.Y = None
        self.Z = None
        self.delta_W = None
        
        self.dropout = dropout
        self.mask = None
        
        if activation=='sigmoid':
            self.f = lambda M: list(map(lambda x:1/(1+np.exp(-x)),M))
            self.df = lambda x: np.multiply(self.Z,1-self.Z)
        else:
            if not activation:
                activation = lambda x:x
            x = sym.Symbol('x')
            f = sym.lambdify('x',activation(x),"numpy")
            df = sym.lambdify('x',activation(x).diff(),"numpy")
            
            self.f = lambda M: list(map(f,M)) #Permet d'utiliser la fonction sur une matrice
            self.df = lambda M: list(map(df,M)) #Permet d'utiliser la fonction sur une matrice
        
    def init_weights(self):
        return np.random.randn(self.isize+1,self.shape)
    
    def dropoutMask(self):
        mask = [np.random.rand()>self.dropout for _ in range(self.shape)]
        return mask
    
    def maskMat(self,M):
        for i,c in enumerate(self.mask):
            if not c:
                M[:,i]=0
    
    def output(self,inp,train=False):
        """
        X -> (batch, n_i + bias)
        W -> (n_i + bias, n_i+1)
        Z -> (batch, ni+1)
        """
        if train:
            self.mask = self.dropoutMask()
        else:
            self.mask = np.ones(self.shape)
        
        X = self.addBias(self.X.output(inp,train=train))
        self.Y = np.dot(X,self.W)
        self.Z = np.array(self.f(self.Y))
        self.maskMat(self.Z)
        return self.Z
    
    def derivate(self,W,err):
        """ 
        Calcule l'erreur pour chaque neurone et chaque observation du batch
        
        old_err -> (batch, n_i+1) \n
        W       -> (n_i + bias,n_i+1)
        dfY     -> (batch, n_i)
        new_err -> (batch, n_i)
        """

        err_pondere = np.dot(err,np.transpose(W))
        dfY = np.array(self.df(self.Y))
        self.maskMat(dfY)
        
        try: #Si la derivee de la fonction d'activation est 1
            iter(dfY[0])
        except:
            dfY = np.ones(self.Y.shape)#[self.df(ligne) for ligne in self.Y]
            
        if len(dfY[0]) != len(err_pondere[0]):
            err_pondere = self.removeBias(err_pondere)
        return np.multiply(err_pondere,dfY)

    def update(self,W,err,coef,regularization=None,momentum=0.):
        """
        Met à jour les poids en calculant le gradient puis le propage aux couches precedantes
        
        new_err -> (batch,n_i)
        Z -> (batch,n_i-1 + bias)
        delta_w_batch -> (batch,n_i-1 + bias,n_i)
        delta_w -> (n_i-1 + bias,n_i)
        """
        
        new_err = self.derivate(W,err)
        Z = self.X.Z
        Z = self.addBias(Z)

        delta_W_batch = [np.outer(z,e) for (z,e) in zip(Z,new_err)]#np.multiply(new_err,Z)

        if self.delta_W is None:
            self.delta_W = 0.
            momentum=0.
        self.delta_W = momentum*self.delta_W + (1-momentum)*np.mean(delta_W_batch,axis=0)

        self.X.update(self.W,new_err,coef,regularization=regularization,momentum=momentum) #Propage l'erreur aux couches precedentes
        
        self.W -= coef*self.delta_W #Met à jour les poids
        
        if regularization:
            if regularization[0] == 'L1':
                self.W -= regularization[1]*np.sign(self.W)
            elif regularization[0] == 'L2':
                self.W -= 2*regularization[1]*self.W
        
        
class Perceptron():
    
    def __init__(self,input_size,output_size,cost_fun,regularization=None):
        
        self.layers=[ConstLayer(input_size)]
        self.regularization=regularization
        
        if cost_fun=='cross' or cost_fun=='cross_entropy' or cost_fun=='cross_entropy_with_softmax':
            self.cost_fun = lambda Ypred,Y: np.sum(Y,axis=1)*np.log([sum(np.exp(s)) for s in Ypred])-np.array([np.dot(ypred,y) for ypred,y in zip(Ypred,Y)])
            self.dcost_fun = lambda Ypred,Y: [softmax(ypred)-y for ypred,y in zip(Ypred,Y)]
        elif cost_fun=='mse' or cost_fun=='MSE':
            self.cost_fun = lambda Ypred,Y: 1/(2*output_size)*np.sum(np.square(Ypred-Y),axis=1)
            self.dcost_fun = lambda Ypred,Y: [ypred-y for ypred,y in zip(Ypred,Y)]
        else:
            cost_fun,dcost_fun = self.getDerivate(cost_fun,output_size)
            self.cost_fun = lambda M,N: list(map(cost_fun,M,N))
            self.dcost_fun = lambda M,N: list(map(dcost_fun,M,N))
    
    def getDerivate(self,fun,output_size):
        """Derive la fonction de coût
        """
        d=output_size
        x = [sym.Symbol(s) for s in ['x'+str(i) for i in range(d)]]
        y = [sym.Symbol(s) for s in ['y'+str(i) for i in range(d)]]
        
        cost_sym = fun(x,y)
        dcost_sym = sym.Array(cost_sym.diff(xi) for xi in x)
        
        cost_lamb = sym.lambdify(np.concatenate((x,y)),cost_sym,'numpy',dummify=False)
        dcost_lamb = sym.lambdify(np.concatenate((x,y)),dcost_sym,'numpy',dummify=False)
        cost_fun = lambda ypred,ytrue : cost_lamb(*ypred,*ytrue)
        dcost_fun = lambda ypred,ytrue : dcost_lamb(*ypred,*ytrue)
        return cost_fun,dcost_fun
    
    def addLayer(self,nsize,activation=None,dropout=0.,name=None):
        self.layers.append(FCLayer(self.layers[-1],nsize,activation=activation,dropout=dropout,name=name))
    
    def output(self,X,train=False):
        return self.layers[-1].output(X,train=train)
        
    def train(self,X,Y,coef=0.1,momentum=0.):
        Ypred=self.output(X,train=True)
        
        if len(Ypred[0]) != len(Y[0]):
            raise(Exception("La dernière couche n'a pas la bonne taille"))
        
        err = self.dcost_fun(Ypred,Y)
        
        W=np.eye(len(err[0]))

        self.layers[-1].update(W,err,coef,regularization=self.regularization,momentum=momentum)
        return np.mean(self.cost_fun(Ypred,Y))
    
    def score(self,X,Y):
        Ypred = self.output(X)
        return sum([int(np.argmax(y)==np.argmax(ypred)) for y,ypred in zip(Y,Ypred)])/len(X)
    
    def getWeights(self):
        return [l.W for l in self.layers[1:]]
    
def sigmoid(x):
    return 1/(1+sym.exp(-x))

def cross_entropy_with_softmax(logits,p):
    exp = np.array([sym.exp(s) for s in logits])
    return sym.Matrix([-sym.log(e/sym.Add(*exp)) for e in exp]).dot(p).expand(logits,force=True)

def softmax(x):
    e = np.array([np.exp(xi) for xi in x])
    return e/sum(e)

def MSE(Ypred,Y):
    return 1/len(Y) * sym.Add(*(np.array(Ypred)-np.array(Y))**2)
















