# -*- coding: utf-8 -*-
'''
Created on 7 juil. 2018

@author: Theo Moutakanni
'''

import numpy as np
import sympy as sym
import matplotlib.pyplot as plt
from tqdm import tqdm

from Perceptron.Perceptron_Albert_Theo.perceptron import Perceptron, cross_entropy_with_softmax, MSE

if __name__ == '__main__':
    
    entry=np.array([[0,0],[0,1],[1,1],[1,0]])-0.5
    output_cross=[[1,0],[0,1],[1,0],[0,1]]
    output_mse=[[0],[1],[0],[1]]
    
    n = 10 #nombre de perceptrons entraînés
    
    error_cross = []
    for i in range(n):
        p_cross=Perceptron(2,2,'cross')
        p_cross.addLayer(4,activation='sigmoid',name="fc1")
        p_cross.addLayer(2,name="fc2")
    
        err = []
        for _ in tqdm(range(5000),leave=False):
            err.append(p_cross.train(entry,output_cross,1))
        error_cross.append(err)
    
    
    error_mse = []
    for i in range(n):
        p_mse=Perceptron(2,1,'MSE')
        p_mse.addLayer(4,activation='sigmoid',name="fc1")
        p_mse.addLayer(1,activation='sigmoid',name="fc2")
        
        err = []
        for _ in tqdm(range(5000),leave=False):
            err.append(p_mse.train(entry,output_mse,1))
        error_mse.append(err)
    
    cross_mean = np.mean(error_cross,axis=0)
    cross_std = np.std(error_cross,axis=0)
    cross_intsup = cross_mean + 1.96*cross_std/np.sqrt(n)
    cross_intinf = cross_mean - 1.96*cross_std/np.sqrt(n)
    
    mse_mean = np.mean(error_mse,axis=0)
    mse_std = np.std(error_mse,axis=0)
    mse_intsup = mse_mean + 1.96*mse_std/np.sqrt(n)
    mse_intinf = mse_mean - 1.96*mse_std/np.sqrt(n)

    plt.plot(mse_mean,'r')
    plt.plot(cross_mean,'b')
    plt.plot(mse_intsup,'r--')
    plt.plot(mse_intinf,'r--')
    plt.plot(cross_intsup,'b--')
    plt.plot(cross_intinf,'b--')
    plt.legend(['Cross-entropy','MSE'])
    plt.show()
    print(p_mse.output(entry))