# -*- coding: utf-8 -*-
'''
Created on 5 oct. 2018

@author: Acer V-NITRO
'''
import matplotlib.pyplot as plt
from tqdm import tqdm
import numpy as np

from Perceptron.Perceptron_Albert_Theo.perceptron import Perceptron, cross_entropy_with_softmax, MSE, softmax, sigmoid

if __name__ == '__main__':

    from sklearn.datasets import load_digits
    digits = load_digits()
    
    from sklearn.model_selection import train_test_split
    x_train, x_test, y_train, y_test = train_test_split(digits.data/16-0.5, [[int(i==y) for i in range(10)] for y in digits.target], test_size=0.25, random_state=0)
    
    def coefEpoch(epoch):
        return 1 if epoch<150 else 0.1
    """
    p1 = Perceptron(8*8,10, 'cross')
    p1.addLayer(200,activation='sigmoid', name='fc1')
    p1.addLayer(100,activation='sigmoid', name='fc2')
    p1.addLayer(10, name='fc4')
    
    err_cross=[]
    acc_cross_test,acc_cross_train=[],[]
    for epoch in tqdm(range(100)):
        err_cross.append(p1.train(x_train, y_train, coef=1, momentum=0.5))
        acc_cross_test.append(p1.score(x_test,y_test))
        acc_cross_train.append(p1.score(x_train,y_train))
    """
    n=1
    acc_p2_test,acc_p2_train = [],[]
    for _ in tqdm(range(n)):
        p2 = Perceptron(8*8,10,'cross',regularization=('L2',0.001))
        p2.addLayer(100,activation='sigmoid', name='fc1')
        p2.addLayer(50,activation='sigmoid', name='fc2')
        p2.addLayer(25,activation='sigmoid', name='fc3')
        p2.addLayer(10, name='fc4')
        
        err=[]
        acc_test,acc_train=[],[]
        for epoch in tqdm(range(200),leave=False):
            err.append(p2.train(x_train, y_train, coef=coefEpoch(epoch),momentum=0.1))
            acc_test.append(p2.score(x_test,y_test))
            acc_train.append(p2.score(x_train,y_train))
        acc_p2_test.append(acc_test)
        acc_p2_train.append(acc_train)
        
    p2_mean_test = np.mean(acc_p2_test,axis=0)
    p2_std_test = np.std(acc_p2_test,axis=0)
    p2_intsup_test = p2_mean_test + 1.96*p2_std_test/np.sqrt(n)
    p2_intinf_test = p2_mean_test - 1.96*p2_std_test/np.sqrt(n)

    p2_mean_train = np.mean(acc_p2_train,axis=0)
    p2_std_train = np.std(acc_p2_train,axis=0)
    p2_intsup_train = p2_mean_train + 1.96*p2_std_train/np.sqrt(n)
    p2_intinf_train = p2_mean_train - 1.96*p2_std_train/np.sqrt(n)

    plt.plot(p2_mean_test,'r')
    plt.plot(p2_mean_train,'b')
    
    plt.plot(p2_intsup_test,'r--')
    plt.plot(p2_intinf_test,'r--')
    plt.plot(p2_intsup_train,'b--')
    plt.plot(p2_intinf_train,'b--')
    plt.legend(['Test','Train'])
    plt.show()
    print(max(p2_mean_train),max(p2_mean_test))
    #print(p2.output(x_test[:5]))