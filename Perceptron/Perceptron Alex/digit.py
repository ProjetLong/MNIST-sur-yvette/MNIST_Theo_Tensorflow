# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 23:25:02 2018

@author: Utilisateur
"""
from xorv2 import Cerveau
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_digits

digits = load_digits()


def f(x):
    return 1/(1 + np.exp(-x))

def fp(x):
    return f(x)*(1-f(x))

#(1797,)
temp = np.eye(10)
temp = [temp[i] for i in range(10)]
for i in range(len(temp)):
    temp[i] = np.array([[temp[i][j]] for j in range(len(temp[i]))])

fin = int(0.75*1797)

XX = (1/16)*digits.data[:fin]
YY = np.array([temp[digits.target[i]] for i in range(fin)])

Xtest = (1/16)*digits.data[fin:]
Ytest = digits.target[fin:]

def newSet(tailleBatch):
    choix = np.arange(fin)
    np.random.shuffle(choix)
    XXX = []
    YYY = []
    for i in range(len(choix)):
        XXX.append(XX[choix[i]])
        YYY.append(YY[choix[i]])
    XXXX = []
    YYYY = []
    i = 0
    while i * tailleBatch < fin:
        XXXX.append(XXX[(i * tailleBatch):min((i + 1) * tailleBatch, fin - 1)])
        YYYY.append(YYY[i * tailleBatch:min((i + 1) * tailleBatch, fin - 1)])
        i = i + 1
    return XXXX,YYYY








def resCerveau(cerveau, elt):
    cerveau.propagation(elt)
    res = np.array([cerveau.couches[-1].neuronnes[i][0] for i in range(len(cerveau.couches[-1].neuronnes))])
    return np.argmax(res)
    

def matriceConfusion(cerveau, Xtest, Ytest):
    mat = np.zeros((10,10))
    for i in range(len(Xtest)):
        mat[resCerveau(cerveau, Xtest[i])][Ytest[i]] += 1
    return mat

def precision(cerveau, Xtest, Ytest):
    pres = 0
    for i in range(len(Xtest)):
        if resCerveau(cerveau, Xtest[i]) == Ytest[i]:
            pres += 1
    return pres / len(Xtest)
    


def iteration(L, n, tailleBatch):
    a = Cerveau(L, [f for i in range(len(L) - 1)], [fp for u in range(len(L) - 1)])
    c = []
    for i in tqdm(range(n)):
        Xdata, Ydata = newSet(tailleBatch)
        for j in range(len(Xdata)):
            a.apprendre(Xdata[j], Ydata[j])
        c.append(precision(a, Xtest, Ytest))
    print(precision(a, Xtest, Ytest))
    return c

def moyenne(n, nb, tailleBatch, L = [64, 16, 16, 10]):
    moy = []
    res = []
    ecart = []
    ecart1 = []
    ecart2 = []
    for i in range(n):
        moy.append(iteration(L, nb, tailleBatch))
    for i in range(len(moy[0])):
        somme = 0
        for j in range(len(moy)):
            somme += moy[j][i]
        somme = somme / n
        res.append(somme)
    for i in range(len(moy[0])):
        temp = []
        for j in range(len(moy)):
            temp.append(moy[j][i])
        ecart.append(np.std(np.array(temp)))
        ecart1.append(res[i]-1.96*ecart[i]/(np.sqrt(n)))
        ecart2.append(res[i]+1.96*ecart[i]/(np.sqrt(n)))
    plt.plot(np.linspace(0,len(moy[0]) - 1, len(moy[0])), res)
    for i in range(len(ecart)):
        if i%10 == 1:
            plt.plot([i, i], [ecart1[i], ecart2[i]], 'b-')
    
    plt.show()
    
    
