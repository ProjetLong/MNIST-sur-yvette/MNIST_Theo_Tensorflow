.. MNIST_Theo_Tensorflow documentation master file, created by
   sphinx-quickstart on Sat Jul  7 01:10:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MNIST_Theo_Tensorflow's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   source/modules
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
