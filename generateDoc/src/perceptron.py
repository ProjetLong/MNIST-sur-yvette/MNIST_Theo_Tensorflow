'''
Created on 7 juil. 2018

@author: Theo Moutakanni

'''

import numpy as np
import sympy

from abc import ABC, abstractmethod

import matplotlib.pyplot as plt
from tqdm import tqdm

from sklearn.datasets import load_digits

class Perceptron(object):
    """Example function with types documented in the docstring.

    Parameters
    ----------
    param1 : int
        The first parameter.
    param2 : str
        The second parameter.

    Returns
    -------
    bool
        True if successful, False otherwise.

    """



    def __init__(self, params):
        pass
            
    def func(self, arg1, arg2):
        """Summary line.
    
        Extended description of function.
    
        Args:
            arg1 (int): Description of arg1
            arg2 (str): Description of arg2
    
        Returns:
            bool: Description of return value
    
        """
        return True
        