# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 00:54:04 2019

@author: Utilisateur
"""


"""
faceCascade = cv2.CascadeClassifier("C:\\Users\\Utilisateur\\Desktop\\Cours\\Lip reader\\script\\face.xml")
mouthCascade = cv2.CascadeClassifier("C:\\Users\\Utilisateur\\Desktop\\Cours\\Lip reader\\script\\smile.xml")



image = cv2.imread("C:\\Users\\Utilisateur\\Desktop\\Cours\\Lip reader\\script\\test.jpg", 0)


faces = faceCascade.detectMultiScale(
    image,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(30, 30)
)


if len(faces) != 1:
    print("Error on vera àa plus tard")
else:
    (x,y,w,h) = faces[0]
    faceImg = image[y:y+h, x:x+w]
    
    
    
smile = mouthCascade.detectMultiScale(
        faceImg,
        scaleFactor = 1.5,
        minNeighbors = 15,
        minSize = (60,60)
)

if len(faces) != 1:
    print("Error on vera àa plus tard")
else:
    (x,y,w,h) = smile[0]
    smileImg = faceImg[y:y+h, x:x+w]

    
    
for (x, y, w, h) in smile:
    cv2.rectangle(faceImg, (x, y), (x+w, y+h), (0, 206, 124), 2)


cv2.imwrite("C:\\Users\\Utilisateur\\Desktop\\Cours\\Lip reader\\script\\marchepas.jpg", faceImg)


cv2.imshow("image", faceImg)
cv2.waitKey(0)
cv2.destroyAllWindows()





"""

import cv2
import sys, os

def dist(a, b, c, d):
    return ((a-c)*2 + (b-d)**2)

def proche(L, x, y):
    mini = dist(L[0][0], L[0][1], x, y)
    retour = (L[0][0], L[0][1], L[0][2], L[0][3])
    for (x1, y1, w, h) in L:
        if dist(x1, y1, x, y) < mini:
            mini = dist(x1, y1, x, y)
            retour = (x1, y1, w, h)
    return retour
            
#Représente le chemin du script
pathname = os.path.dirname(sys.argv[0])        
path = os.path.abspath(pathname)            

##Définition des fichiers qui seront utiles pour analyser la vidéo
nomVideo = input("Nom fichier avec extension ?\n")
faceCascade = cv2.CascadeClassifier(path + "\\face.xml")
mouthCascade = cv2.CascadeClassifier(path + "\\mouth.xml")
cap = cv2.VideoCapture(path + '\\' + nomVideo)

#Traitement de la vidéo frame par frame
i = 1
frameFinale = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
frameFinale = 200
xold, yold, maxh, maxw = 0, 0, 0, 0
if not cap.isOpened():
    print("Le fichier n'existe pas")    
    
    
while(cap.isOpened() and i < frameFinale):      #Si on ne veut pas traiter toute la vidéo on règle fraleFinale
    
    #Lecture de la frame et transformation de l'image en niveau de gris
    ret, frame = cap.read()
    image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    print(str((i/frameFinale)*100) + "%")
    
    #Détection des figures sur la vidéo (il n'en faut qu'une seule)
    faces = faceCascade.detectMultiScale(
        image,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
    )
    
    #S'il y a plus d'une figure ou pas on l'enregistre dans le dossier erreur
    if len(faces) == 0:
        cv2.imwrite(path + "\\err\\face" + str(i) + ".jpg", image)
        continue
    elif len(faces) > 1:
        faces[0] = proche(faces, xold, yold)
        continue
    
    #On crop la figure pour ne garder que le bas de la figure
    (x,y,w,h) = faces[0]
    h = int(h/2)
    y = int(y + h + 20)
    faceImg = image[y:y+h, x:x+w]
        
    #On cherche toute les bouches présente sur l'image
    smile = mouthCascade.detectMultiScale(
        faceImg,
        scaleFactor = 1.1,
        minNeighbors = 40
        )
        
    #Il faut une seule bouche exactement
    if len(smile) != 1:
        for (x, y, w, h) in smile:
            cv2.rectangle(faceImg, (x, y), (x+w, y+h), (0, 206, 124), 2)
        cv2.imwrite(path + "\\err\\face" + str(i) + ".jpg", faceImg)
        continue
    
    
    (x,y,w,h) = smile[0]
    smilepic = faceImg[y:y+h, x:x+w]
    
    #On choppe la taille maximale d'un image
    maxw = max(maxw, w)
    maxh = max(maxh, h)
    
    #Sauvegarde de la bouche dans le dossier res
    cv2.imwrite(path + "\\res\\frame" + str(i) + ".jpg", smilepic)
    i += 1


#On resize toutes les images pour les avoir à la même taille
rz = None
while rz != "y" or rz != "n":
    rz = input("Resize les images ? y/n")
    
if rz == "y":    
    for j in range(1, i):
        image = cv2.imread(path + "\\res\\frame" + str(j) + ".jpg")
        newImage = cv2.resize(image, (int(maxw), int(maxh)))
        cv2.imwrite(path + "\\res\\frame" + str(j) + ".jpg", newImage)

#On ferme toutes les instances pour éviter les fuites de mémoire
cap.release()
cv2.destroyAllWindows()

