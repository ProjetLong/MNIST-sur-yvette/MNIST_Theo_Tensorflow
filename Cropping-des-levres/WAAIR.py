# -*- coding: utf-8 -*-
"""
Created on Sat Feb 16 21:21:32 2019

@author: Utilisateur
"""


from tqdm import tqdm
import sys, os, cv2

pathname = os.path.dirname(sys.argv[0])        
path = os.path.abspath(pathname)

pathname = input("Chemin du dossier contenant les images à resize (Chemin relatif)\n")

#On récupère la liste des fichiers image dans le dossier sélectionné
files = os.listdir(pathname)
files = list(filter(lambda file: file[-4:] == ".jpg", files))


maxh, maxw = 0, 0
for file in tqdm(files):
    image = cv2.imread(pathname + "\\" + file)
    h, w, _ = image.shape
    maxw = max(maxw, w)
    maxh = max(maxh, h)
    
for file in tqdm(files):
    image = cv2.imread(pathname + "\\" + file)
    newImage = cv2.resize(image, (int(maxw), int(maxh)))
    cv2.imwrite(pathname + "\\" + file, newImage)
